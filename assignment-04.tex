\documentclass[11pt]{article}
\usepackage[table,xcdraw]{xcolor}
\usepackage[english]{babel}
\usepackage{natbib}
\usepackage{hyperref}
\usepackage{url}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage{parskip}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{vmargin}
\usepackage{float}
\usepackage[font=footnotesize]{subfig}
\usepackage{tabularx}
\usepackage{xcolor}
\usepackage{listings}
\lstset{basicstyle=\small,
	showstringspaces=false,
	commentstyle=\color{red},
	keywordstyle=\color{blue}
}
\setmarginsrb{3 cm}{0.5 cm}{3 cm}{0.5 cm}{1 cm}{1.5 cm}{1 cm}{1.5 cm}

\title{KTR-GIK-M}
%\ Put your group name here
\author{Group S}
\date{Bamberg \today}

\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\makeatother

\pagestyle{fancy}
\fancyhf{}
\rhead{\theauthor}
\lhead{\thetitle}
\cfoot{\thepage}

\newcommand{\onefigure}[4]{%
	\begin{figure}[H]%
		\centering%
		\includegraphics[width=#1\textwidth]{figures/#2}%
		\caption{#3}%
		\label{#4}%
	\end{figure}%
}

\newcommand{\twofigures}[9]{%
	\begin{figure}[H]%
		\centerline{%
			\subfloat[#3]{%
				\includegraphics[width=#1\textwidth]{figures/#2}%
				\label{#4}%
			}%
			\hfill%
			\subfloat[#6]{%
				\includegraphics[width=#1\textwidth]{figures/#5}%
				\label{#7}%				
			}%
		}%
		\caption{#8}%
		\label{#9}%
	\end{figure}%
}

\begin{document}
	
	\begin{titlepage}
		\centering
		\vspace*{0.5 cm}
		\includegraphics[scale=0.5]{images/uni_ba_logo.png}\\[1.0 cm]
		\textsc{\LARGE Otto-Friedrich-University of Bamberg}\\[1.0 cm]
		\textsc{\Large Professorship for Computer Science,}\\[0.2 cm]
		\textsc{Communication Services, Telecommunication}\\[0.2 cm]
		\textsc{Foundations of Internet Communication}\\[1.0 cm]
		\rule{\linewidth}{0.2 mm} \\[0.4 cm]
		{ \LARGE \bfseries \thetitle}\\
		\rule{\linewidth}{0.2 mm} \\[1.0 cm]
		\textsc{\large Assignment 4}\\[2.0 cm]
		Submitted by:\\
		\textbf{\theauthor}\\[0.5 cm]
		\begin{minipage}{0.4\textwidth}
			\begin{flushleft} \large
				\emph{Author:}\\
				Tasfia Sharmin\\
				Jewel Datta\\
				Mohammad Ismail Hossen\\
				Philipp Huber\\
				Aroon Kumar\\
				
			\end{flushleft}
		\end{minipage}~
		\begin{minipage}{0.4\textwidth}
			\begin{flushright} \large
				\emph{Student Number:} \\
				2064899\\
				2060538\\
				2053447\\
				2066681\\
				1972088\\
				
			\end{flushright}
		\end{minipage}\\[2 cm]
		Supervisor: : Prof. Dr. Udo Krieger\\
		{\thedate}\\
		Summer Term 2021\\
		\vfill
	\end{titlepage}
	
	\pagenumbering{roman}
	\tableofcontents
	\listoffigures
	
	
	\pagenumbering{arabic}
	
	%\onefigure{0.5}{tux.png}{The incredible tux}{img: bigtux}
	
	%\twofigures{0.4}{tux-1.png}{The Tux 1}{img: tux-1}{tux-2.png}{The Tux 2}{img: tux-2}{The Tux Guys}{img: tuxe}
	%\ include your section report here
	\input{contents/section-1}
	\input{contents/section-2}
	\input{contents/section-3}
	\clearpage
	%\input{contents/timeline.tex}
	\bibliographystyle{plain}
	\bibliography{assignment-references}
\end{document}

%https://superuser.com/questions/684275/how-to-forward-packets-between-two-interfaces
%https://serverfault.com/questions/453254/routing-between-two-networks-on-linux
%http://linux-ip.net/html/tools-ip-route.html
%https://www.cisco.com/c/en/us/td/docs/security/asa/asa72/configuration/guide/conf_gd/intparam.html
%http://jodies.de/ipcalc
%http://www.ciscopress.com/articles/article.asp?p=2181836&seqNum=4
%http://docs.ansible.com/ansible/latest/modules/vyos_static_route_module.html
%https://openmaniak.com/vyatta_case1.php
